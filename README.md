<div class="container readme-background" id="container_readme">
  <div class="readme">
  <h2>古歌云 | sctoCloud 信息更新</h2>
    <p>本页面用于发布最新信息, 请按 CTRL+D 收藏本页面 或 星标本项目</p>
    <p>请同时收藏下列网址 作为备份:</p>
    <p>https://github.com/sctoCloud/info</p>
    
  <ul>
    <h3>最新域名:</h3>
      https://www.sctocloud.com
    <h3>永久域名:</h3>
      https://scto.xyz
    <h3>本以上方法均失效时, 可联系管理员邮箱:</h3>
    sctocare@protonmail.com
  </ul>
  </div>
</div>

---
---

## 公告 | Announcement

### [通知: 庆祝蛇年活动现已开启]
### **庆祝 农历新年暨岁末活动 全站半年付(含)以上85折**

#### ⚠️购买前请认真阅读下面几点说明！

#### ⚠️套餐无法叠加，如果当前账户有套餐，一定要先折算现有套餐再用优惠码买新套餐！！！
#### ⚠️如已买了新套餐，才发现之前老套餐没折算，直接找客服帮忙处理，禁止自行折算！

折算流程：
1. 点击 https://www.sctocloud.com/user/code 进入钱包界面
2. 下滑到底部 `套餐记录` ，点击现有套餐旁的齿轮可将现套餐折算为余额

购买时请输入:
`240128IRA3gQqJ`
额度：15% off; 半年付, 年付85折
有效期至: 2025-02-17 11:00 AM (UTC+8)

每位用户活动期间仅限使用一次!
